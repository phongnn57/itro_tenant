package meboo.com.itrouser.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.google.gson.JsonObject;

import meboo.com.itrouser.Key;
import meboo.com.itrouser.network.NetworkModule;
import meboo.com.itrouser.ui.activity.LoginActivity;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Blue on 7/10/2017.
 */

public class Logout {
    private Logout() {

    }

    public static void logout(final Context context) {
        Log.i("SettingFragment", "logout: ");
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Đăng xuất");
        builder.setMessage("Bạn phải đăng nhập lại!");
        builder.setPositiveButton("Đăng xuất", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, int i) {

                String token = "Bearer " + PrefUtil.getDataUser(context).getToken();
                NetworkModule.getService().logout(token, "android")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<JsonObject>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.i("SettingFragment", "onError: " + e);
                            }

                            @Override
                            public void onNext(JsonObject jsonObject) {
                                Log.i("SettingFragment", "onNext: " + jsonObject);
                                if (jsonObject.get("status").getAsInt() == 1) {
                                    String password = PrefUtil.getString(context, Key.PASS_WORD, "");
                                    String username = PrefUtil.getString(context, Key.USER_NAME, "");
                                    PrefUtil.delete(context, Key.APP_PREFERENCE);
                                    PrefUtil.delete(context, Key.FILTER);
                                    PrefUtil.saveString(context, Key.USER_NAME, username);
                                    PrefUtil.saveString(context, Key.PASS_WORD, password);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    context.startActivity(intent);
                                    ((Activity) context).finish();

                                    dialogInterface.dismiss();
                                }
                            }
                        });
            }
        });
        builder.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
