package meboo.com.itrouser.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;

import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseFragment;
import meboo.com.itrouser.presenter.HomePresenter;
import meboo.com.itrouser.presenter.impl.HomePresenterImpl;
import meboo.com.itrouser.ui.activity.ChiTietHoaDonActivity;
import meboo.com.itrouser.ui.activity.DanhSachHoaDonActivity;
import meboo.com.itrouser.ui.activity.LichSuDongTienActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment<HomePresenter> implements HomeView, View.OnClickListener {
    ImageView imvDsHoaDon, imvLichSuDongTien, imvKeKhaiTamTru;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_home;
    }

    @Override
    public void initializeComponents(View view) {
        findViewByIds(view);

    }

    public void findViewByIds(View view) {
        imvDsHoaDon = view.findViewById(R.id.imv_ds_hoa_don);
        imvLichSuDongTien = view.findViewById(R.id.imv_lich_su_dong_tien);
        imvKeKhaiTamTru = view.findViewById(R.id.imv_ke_khai_tam_tru);
        imvDsHoaDon.setOnClickListener(this);
        imvLichSuDongTien.setOnClickListener(this);
        imvKeKhaiTamTru.setOnClickListener(this);
    }

    @Override
    public HomePresenter createPresenter() {
        return new HomePresenterImpl(this);
    }

    @Override
    public Context gContext() {
        return getContext();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_ds_hoa_don:
                Intent intent = new Intent(getActivity(), DanhSachHoaDonActivity.class);
                startActivity(intent);
                break;
            case R.id.imv_lich_su_dong_tien:
                Intent intent2 = new Intent(getActivity(), LichSuDongTienActivity.class);
                startActivity(intent2);
                break;
            case R.id.imv_ke_khai_tam_tru:
                Intent intent3 = new Intent(getActivity(), ChiTietHoaDonActivity.class);
                startActivity(intent3);
                break;
        }
    }
}
