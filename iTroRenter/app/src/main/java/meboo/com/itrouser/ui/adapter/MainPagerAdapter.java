package meboo.com.itrouser.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import meboo.com.itrouser.ui.fragment.BlankFrag;
import meboo.com.itrouser.ui.fragment.ChatFragment;
import meboo.com.itrouser.ui.fragment.HomeFragment;
import meboo.com.itrouser.ui.fragment.SettingFragment;

/**
 * Created by VietVan on 03/07/2018.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {

    public static final int HOME_INDEX = 0;
    public static final int CHAT_INDEX = 1;
    public static final int SETTING_INDEX = 2;
    public static final int MAX_PAGES = 3;
    public BlankFrag blankFrag;
    public HomeFragment homeFragment;
    public SettingFragment settingFragment;
    public ChatFragment chatFragment;

//    public DashboardFragment dashboardFragment;

    private boolean check;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setNotifFragment(boolean check) {
        this.check = check;
    }

    public boolean isCheck() {
        return check;
    }

    @Override
    public Fragment getItem(int position) {
        blankFrag=new BlankFrag();
        switch (position) {
            case HOME_INDEX:
                homeFragment = HomeFragment.newInstance();
                return homeFragment;
            case CHAT_INDEX:
                chatFragment = ChatFragment.newInstance();
                return chatFragment;
            case SETTING_INDEX:
                settingFragment = SettingFragment.newInstance();
                return settingFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return MAX_PAGES;
    }

}
