package meboo.com.itrouser.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;

/**
 * Created by xuanhong on 11/16/17. HappyCoding!
 */

public class MoneyInputEditText extends TextInputEditText {
    private final int MAX_LENGTH = 19;

    public MoneyInputEditText(Context context) {
        super(context);

        init();
    }

    public MoneyInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public MoneyInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        addTextChangedListener(new MoneyNumberFormattingTextWatcher(this, MAX_LENGTH));

        InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(MAX_LENGTH);
        setFilters(fArray);
    }

    public @NonNull
    Long getMoney() {
        String cleanString = super.getText().toString();
        cleanString = cleanString.replaceAll("[$,.]", "");
        long parse;
        try {
            parse = Long.parseLong(cleanString);
        } catch (NumberFormatException e) {
            parse = 0L;
        }
        return parse;
    }
}