package meboo.com.itrouser.presenter.impl;

import meboo.com.itrouser.base.BasePresenterImpl;
import meboo.com.itrouser.presenter.LichSuDongTienPresenter;
import meboo.com.itrouser.ui.activity.LichSuDongTienView;

public class LichSuDongTienPresenterImp extends BasePresenterImpl<LichSuDongTienView> implements LichSuDongTienPresenter {
    public LichSuDongTienPresenterImp(LichSuDongTienView view) {
        super(view);
    }
}
