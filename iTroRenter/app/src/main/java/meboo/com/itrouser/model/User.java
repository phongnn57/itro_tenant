package meboo.com.itrouser.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("facebook_id")
    @Expose
    private Object facebookId;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("province_id")
    @Expose
    private String provinceId;
    @SerializedName("district_id")
    @Expose
    private String districtId;
    @SerializedName("ward_id")
    @Expose
    private String wardId;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("id_number")
    @Expose
    private Object idNumber;
    @SerializedName("temp_residence")
    @Expose
    private Object tempResidence;
    @SerializedName("residence")
    @Expose
    private Object residence;
    @SerializedName("folk")
    @Expose
    private Object folk;
    @SerializedName("nation")
    @Expose
    private Object nation;
    @SerializedName("religion")
    @Expose
    private Object religion;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("collect")
    @Expose
    private Integer collect;
    @SerializedName("spend")
    @Expose
    private Integer spend;
    @SerializedName("register_by")
    @Expose
    private Integer registerBy;
    @SerializedName("number_hostels")
    @Expose
    private Integer numberHostels;
    @SerializedName("number_rooms")
    @Expose
    private Integer numberRooms;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("current_room")
    @Expose
    private Object currentRoom;
    @SerializedName("current_hostel")
    @Expose
    private Object currentHostel;
    @SerializedName("join_date")
    @Expose
    private Object joinDate;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("is_owner_first")
    @Expose
    private Integer isOwnerFirst;
    @SerializedName("android_token")
    @Expose
    private String androidToken;
    @SerializedName("ios_token")
    @Expose
    private Object iosToken;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("status_email")
    @Expose
    private Integer statusEmail;
    @SerializedName("status_phone")
    @Expose
    private Integer statusPhone;
    @SerializedName("academic_level")
    @Expose
    private Object academicLevel;
    @SerializedName("job")
    @Expose
    private Object job;
    @SerializedName("job_address")
    @Expose
    private Object jobAddress;
    @SerializedName("temp_residence_province")
    @Expose
    private Object tempResidenceProvince;
    @SerializedName("temp_residence_district")
    @Expose
    private Object tempResidenceDistrict;
    @SerializedName("temp_residence_ward")
    @Expose
    private Object tempResidenceWard;
    @SerializedName("residence_province")
    @Expose
    private Object residenceProvince;
    @SerializedName("residence_district")
    @Expose
    private Object residenceDistrict;
    @SerializedName("residence_ward")
    @Expose
    private Object residenceWard;
    @SerializedName("status_info")
    @Expose
    private Integer statusInfo;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("first_add_renter")
    @Expose
    private Integer firstAddRenter;
    @SerializedName("send_notification_first")
    @Expose
    private Integer sendNotificationFirst;
    @SerializedName("date_send_notification")
    @Expose
    private Object dateSendNotification;
    @SerializedName("is_connect_facebook")
    @Expose
    private Integer isConnectFacebook;
    @SerializedName("is_connect_google")
    @Expose
    private Integer isConnectGoogle;
    @SerializedName("google_id")
    @Expose
    private Object googleId;
    @SerializedName("expire_date_owner")
    @Expose
    private Object expireDateOwner;
    @SerializedName("is_fb_first")
    @Expose
    private Integer isFbFirst;
    @SerializedName("token_active")
    @Expose
    private String tokenActive;
    @SerializedName("token_forget_password")
    @Expose
    private Object tokenForgetPassword;
    @SerializedName("webpush_token")
    @Expose
    private Object webpushToken;
    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    @SerializedName("district_name")
    @Expose
    private String districtName;
    @SerializedName("ward_name")
    @Expose
    private String wardName;
    @SerializedName("hostel")
    @Expose
    private Object hostel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(Object facebookId) {
        this.facebookId = facebookId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getWardId() {
        return wardId;
    }

    public void setWardId(String wardId) {
        this.wardId = wardId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Object getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Object idNumber) {
        this.idNumber = idNumber;
    }

    public Object getTempResidence() {
        return tempResidence;
    }

    public void setTempResidence(Object tempResidence) {
        this.tempResidence = tempResidence;
    }

    public Object getResidence() {
        return residence;
    }

    public void setResidence(Object residence) {
        this.residence = residence;
    }

    public Object getFolk() {
        return folk;
    }

    public void setFolk(Object folk) {
        this.folk = folk;
    }

    public Object getNation() {
        return nation;
    }

    public void setNation(Object nation) {
        this.nation = nation;
    }

    public Object getReligion() {
        return religion;
    }

    public void setReligion(Object religion) {
        this.religion = religion;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getCollect() {
        return collect;
    }

    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    public Integer getSpend() {
        return spend;
    }

    public void setSpend(Integer spend) {
        this.spend = spend;
    }

    public Integer getRegisterBy() {
        return registerBy;
    }

    public void setRegisterBy(Integer registerBy) {
        this.registerBy = registerBy;
    }

    public Integer getNumberHostels() {
        return numberHostels;
    }

    public void setNumberHostels(Integer numberHostels) {
        this.numberHostels = numberHostels;
    }

    public Integer getNumberRooms() {
        return numberRooms;
    }

    public void setNumberRooms(Integer numberRooms) {
        this.numberRooms = numberRooms;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Object currentRoom) {
        this.currentRoom = currentRoom;
    }

    public Object getCurrentHostel() {
        return currentHostel;
    }

    public void setCurrentHostel(Object currentHostel) {
        this.currentHostel = currentHostel;
    }

    public Object getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Object joinDate) {
        this.joinDate = joinDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getIsOwnerFirst() {
        return isOwnerFirst;
    }

    public void setIsOwnerFirst(Integer isOwnerFirst) {
        this.isOwnerFirst = isOwnerFirst;
    }

    public String getAndroidToken() {
        return androidToken;
    }

    public void setAndroidToken(String androidToken) {
        this.androidToken = androidToken;
    }

    public Object getIosToken() {
        return iosToken;
    }

    public void setIosToken(Object iosToken) {
        this.iosToken = iosToken;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getStatusEmail() {
        return statusEmail;
    }

    public void setStatusEmail(Integer statusEmail) {
        this.statusEmail = statusEmail;
    }

    public Integer getStatusPhone() {
        return statusPhone;
    }

    public void setStatusPhone(Integer statusPhone) {
        this.statusPhone = statusPhone;
    }

    public Object getAcademicLevel() {
        return academicLevel;
    }

    public void setAcademicLevel(Object academicLevel) {
        this.academicLevel = academicLevel;
    }

    public Object getJob() {
        return job;
    }

    public void setJob(Object job) {
        this.job = job;
    }

    public Object getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(Object jobAddress) {
        this.jobAddress = jobAddress;
    }

    public Object getTempResidenceProvince() {
        return tempResidenceProvince;
    }

    public void setTempResidenceProvince(Object tempResidenceProvince) {
        this.tempResidenceProvince = tempResidenceProvince;
    }

    public Object getTempResidenceDistrict() {
        return tempResidenceDistrict;
    }

    public void setTempResidenceDistrict(Object tempResidenceDistrict) {
        this.tempResidenceDistrict = tempResidenceDistrict;
    }

    public Object getTempResidenceWard() {
        return tempResidenceWard;
    }

    public void setTempResidenceWard(Object tempResidenceWard) {
        this.tempResidenceWard = tempResidenceWard;
    }

    public Object getResidenceProvince() {
        return residenceProvince;
    }

    public void setResidenceProvince(Object residenceProvince) {
        this.residenceProvince = residenceProvince;
    }

    public Object getResidenceDistrict() {
        return residenceDistrict;
    }

    public void setResidenceDistrict(Object residenceDistrict) {
        this.residenceDistrict = residenceDistrict;
    }

    public Object getResidenceWard() {
        return residenceWard;
    }

    public void setResidenceWard(Object residenceWard) {
        this.residenceWard = residenceWard;
    }

    public Integer getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(Integer statusInfo) {
        this.statusInfo = statusInfo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getFirstAddRenter() {
        return firstAddRenter;
    }

    public void setFirstAddRenter(Integer firstAddRenter) {
        this.firstAddRenter = firstAddRenter;
    }

    public Integer getSendNotificationFirst() {
        return sendNotificationFirst;
    }

    public void setSendNotificationFirst(Integer sendNotificationFirst) {
        this.sendNotificationFirst = sendNotificationFirst;
    }

    public Object getDateSendNotification() {
        return dateSendNotification;
    }

    public void setDateSendNotification(Object dateSendNotification) {
        this.dateSendNotification = dateSendNotification;
    }

    public Integer getIsConnectFacebook() {
        return isConnectFacebook;
    }

    public void setIsConnectFacebook(Integer isConnectFacebook) {
        this.isConnectFacebook = isConnectFacebook;
    }

    public Integer getIsConnectGoogle() {
        return isConnectGoogle;
    }

    public void setIsConnectGoogle(Integer isConnectGoogle) {
        this.isConnectGoogle = isConnectGoogle;
    }

    public Object getGoogleId() {
        return googleId;
    }

    public void setGoogleId(Object googleId) {
        this.googleId = googleId;
    }

    public Object getExpireDateOwner() {
        return expireDateOwner;
    }

    public void setExpireDateOwner(Object expireDateOwner) {
        this.expireDateOwner = expireDateOwner;
    }

    public Integer getIsFbFirst() {
        return isFbFirst;
    }

    public void setIsFbFirst(Integer isFbFirst) {
        this.isFbFirst = isFbFirst;
    }

    public String getTokenActive() {
        return tokenActive;
    }

    public void setTokenActive(String tokenActive) {
        this.tokenActive = tokenActive;
    }

    public Object getTokenForgetPassword() {
        return tokenForgetPassword;
    }

    public void setTokenForgetPassword(Object tokenForgetPassword) {
        this.tokenForgetPassword = tokenForgetPassword;
    }

    public Object getWebpushToken() {
        return webpushToken;
    }

    public void setWebpushToken(Object webpushToken) {
        this.webpushToken = webpushToken;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public Object getHostel() {
        return hostel;
    }

    public void setHostel(Object hostel) {
        this.hostel = hostel;
    }
}

