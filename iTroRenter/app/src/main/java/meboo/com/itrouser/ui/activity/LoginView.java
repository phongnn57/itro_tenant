package meboo.com.itrouser.ui.activity;


import android.app.Activity;
import android.content.Context;

import meboo.com.itrouser.base.BaseView;
import meboo.com.itrouser.presenter.LoginPresenter;

public interface LoginView extends BaseView<LoginPresenter> {
    void addWait();

    void removeWait();

    void selectTab(int i);

    Context gContext();

    void login();

    void showLogin();

    void onFailure(String mes);

    void onSuccess(String mes);

    void dismissErro();

    String getUser();

    void requestFocus(String key);

    String getPassword();

    Activity getPrActivity();
}
