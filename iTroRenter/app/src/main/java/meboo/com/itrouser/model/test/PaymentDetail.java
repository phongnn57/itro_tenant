package meboo.com.itrouser.model.test;

public class PaymentDetail {
    String count, content, amount;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public PaymentDetail(String count, String content, String amount) {

        this.count = count;
        this.content = content;
        this.amount = amount;
    }
}
