package meboo.com.itrouser.util;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class RobotoTextView extends android.support.v7.widget.AppCompatTextView {

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        setTypeface(typeface);
    }

}
