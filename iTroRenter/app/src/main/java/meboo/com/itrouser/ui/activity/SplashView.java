package meboo.com.itrouser.ui.activity;

import android.content.Context;

import meboo.com.itrouser.base.BaseView;
import meboo.com.itrouser.presenter.SplashPresenter;

public interface SplashView extends BaseView<SplashPresenter> {
    Context gContext();

    void login();

    void showLogin();
}
