package meboo.com.itrouser.presenter;

import meboo.com.itrouser.base.BasePresenter;

public interface SplashPresenter extends BasePresenter {
    void reFreshToken();
}
