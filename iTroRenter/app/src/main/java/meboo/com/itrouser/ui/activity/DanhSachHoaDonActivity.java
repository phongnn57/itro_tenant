package meboo.com.itrouser.ui.activity;

import android.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseActivity;
import meboo.com.itrouser.model.test.Payment;
import meboo.com.itrouser.model.test.PaymentMonth;
import meboo.com.itrouser.presenter.DanhSachHoaDonPresenter;
import meboo.com.itrouser.presenter.impl.DanhSachHoaDonPresenterImp;
import meboo.com.itrouser.ui.adapter.DanhSachHoaDonAdapter;
import meboo.com.itrouser.util.Toolbox;

public class DanhSachHoaDonActivity extends BaseActivity<DanhSachHoaDonPresenter> implements DanhSachHoaDonView, View.OnClickListener {
    Toolbar myToolbar;
    RecyclerView rvDsHoaDon;
    ImageView imvFilter;
    String startDate;
    String endDate;
    DanhSachHoaDonAdapter danhSachHoaDonAdapter;
    ArrayList<PaymentMonth> arrayPaymentMonth = new ArrayList<>();

    @Override
    public int getContentViewId() {
        return R.layout.activity_danh_sach_hoa_don;
    }

    @Override
    public void initializeComponents() {
        init();
        setFakeData();
        danhSachHoaDonAdapter = new DanhSachHoaDonAdapter(this, arrayPaymentMonth);
        rvDsHoaDon.setAdapter(danhSachHoaDonAdapter);
    }

    private void setFakeData() {
        ArrayList<Payment> arrayPayment = new ArrayList<>();
        arrayPayment.add(new Payment(10101010, 5000000, 2000000));
        arrayPayment.add(new Payment(10202020, 6000000, 0));
        ArrayList<Payment> arrayPayment2 = new ArrayList<>();
        arrayPayment2.add(new Payment(11111210, 500000, 0));
        arrayPayment2.add(new Payment(10288999, 200000, 0));

        arrayPaymentMonth.add(new PaymentMonth("8/2018", arrayPayment));
        arrayPaymentMonth.add(new PaymentMonth("9/2018", arrayPayment2));
    }

    private void init() {
        rvDsHoaDon = findViewById(R.id.rv_ds_hoa_don);
        myToolbar = findViewById(R.id.myToolbar);
        imvFilter = findViewById(R.id.imv_filter);

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rvDsHoaDon.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        imvFilter.setOnClickListener(this);
    }

    @Override
    public DanhSachHoaDonPresenter createPresenter() {
        return new DanhSachHoaDonPresenterImp(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imv_filter:
                showDialog();
                break;
        }
    }

    private void showDialog() {
        Calendar now = Calendar.getInstance();
        int month = now.get(Calendar.MONTH) + 1;
        int year = now.get(Calendar.YEAR);
        int startMonth, startYear;
        if (month - 3 > 0) {
            startMonth = month - 3;
            startYear = year;

        } else {
            startMonth = 12 + (month - 3);
            startYear = year - 1;
        }

        startDate = startMonth + "/" + startYear;
        endDate = month + "/" + year;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_filter_payment, null);
        builder.setView(view);
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();

        RadioButton rbAll = view.findViewById(R.id.rb_all);
        RadioButton rbPaid = view.findViewById(R.id.rb_paid);
        RadioButton rbUnpaid = view.findViewById(R.id.rb_unpaid);
        RadioGroup radioGroup = view.findViewById(R.id.radio_group);
        TextView txtDateFrom = view.findViewById(R.id.txt_date_from);
        TextView txtDateTo = view.findViewById(R.id.txt_date_to);
        ImageView btnDatePickerFrom = view.findViewById(R.id.btn_date_picker_from);
        ImageView btnDatePickerTo = view.findViewById(R.id.btn_date_picker_to);

        rbAll.setChecked(true);

        txtDateFrom.setText(startDate);
        txtDateTo.setText(endDate);
        btnDatePickerFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toolbox.showDatePickerMonthYearDialog(DanhSachHoaDonActivity.this, null, null, new Toolbox.OnDateSetListener() {
                    @Override
                    public void onDateSet(int year, int month, int dayOfMonth, Calendar calendar) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
                        startDate = dateFormat.format(calendar.getTime());
                        txtDateFrom.setText(startDate);
                    }
                });
            }
        });

        btnDatePickerTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toolbox.showDatePickerMonthYearDialog(DanhSachHoaDonActivity.this, null, null, new Toolbox.OnDateSetListener() {
                    @Override
                    public void onDateSet(int year, int month, int dayOfMonth, Calendar calendar) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
                        endDate = dateFormat.format(calendar.getTime());
                        txtDateTo.setText(endDate);
                    }
                });
            }
        });

        view.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}