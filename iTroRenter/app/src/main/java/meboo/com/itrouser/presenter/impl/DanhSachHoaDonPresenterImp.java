package meboo.com.itrouser.presenter.impl;

import meboo.com.itrouser.base.BasePresenterImpl;
import meboo.com.itrouser.presenter.DanhSachHoaDonPresenter;
import meboo.com.itrouser.ui.activity.DanhSachHoaDonView;

public class DanhSachHoaDonPresenterImp extends BasePresenterImpl<DanhSachHoaDonView> implements DanhSachHoaDonPresenter {
    public DanhSachHoaDonPresenterImp(DanhSachHoaDonView view) {
        super(view);
    }
}
