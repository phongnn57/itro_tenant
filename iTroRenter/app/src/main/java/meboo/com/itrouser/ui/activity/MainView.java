package meboo.com.itrouser.ui.activity;

import meboo.com.itrouser.base.BaseView;
import meboo.com.itrouser.presenter.MainPresenter;

/**
 * Created by VietVan on 03/07/2018.
 */

public interface MainView extends BaseView<MainPresenter> {
    void HomeClick();


    void ChatClick();


    void SettingClick();
}
