package meboo.com.itrouser.base;

public interface BaseView<BPresenter extends BasePresenter> {
    BPresenter getPresenter();

    BPresenter createPresenter();

}
