package meboo.com.itrouser.network;


import com.google.gson.JsonObject;

import java.util.Map;

import meboo.com.itrouser.model.Account;
import meboo.com.itrouser.network.model.ResponeUser;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by HP on 7/24/2017.
 */

public interface NetworkService {

    @POST("/api/v1/login")
    @FormUrlEncoded
    Observable<Account> login(@FieldMap Map<String, String> params);

    @POST("api/v1/account/refresh-token")
    Observable<JsonObject> refreshToken(@Header("Authorization") String token);
    @GET("api/v1/util/province")
    Observable<retrofit2.Response<JsonObject>> province(@Header("Authorization") String token);

    @GET("api/v1/util/district")
    Observable<retrofit2.Response<JsonObject>> district(@Header("Authorization") String token, @Query("province_id") String provinceId);

    @GET("api/v1/util/ward")
    Observable<retrofit2.Response<JsonObject>> ward(@Header("Authorization") String token, @Query("district_id") String districtId);
    @Multipart
    @POST("api/v1/account/register")
    Observable<JsonObject> registerAccount(@Part MultipartBody.Part image,
                                           @Part("first_name") RequestBody firstName,
                                           @Part("last_name") RequestBody lastName,
                                           @Part("gender") RequestBody gender,
                                           @Part("phone") RequestBody phone,
                                           @Part("password") RequestBody password,
                                           @Part("email") RequestBody email,
                                           @Part("birthday") RequestBody birthday,
                                           @Part("province_id") RequestBody provinceId,
                                           @Part("district_id") RequestBody districtId,
                                           @Part("ward_id") RequestBody wardId,
                                           @Part("address") RequestBody address,
                                           @Part("type") int type);

    @FormUrlEncoded
    @POST("api/v1/account/logout")
    Observable<JsonObject> logout(@Header("Authorization") String token, @Field("type") String type);


    @GET("api/v1/account/detail")
    Observable<ResponeUser> getUserDetail(@Header("Authorization") String token, @Query("user_id") int userId);




}
