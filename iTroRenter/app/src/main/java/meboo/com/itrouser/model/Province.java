package meboo.com.itrouser.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tnhoa on 2/6/2018.
 */

public class Province implements Serializable {
    @SerializedName("provinceid")
    @Expose
    private String provinceid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;

    public Province() {
    }

    @Override
    public String toString() {
        return "Province{" +
                "provinceid='" + provinceid + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                '}';
    }

    public String getProvinceid() {
        return provinceid;
    }

    public void setProvinceid(String provinceid) {
        this.provinceid = provinceid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLat() {
        if (lat == null) return 0.0;
        return Double.parseDouble(lat);
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public double getLng() {
        if (lng==null) return 0.0;
        return Double.parseDouble(lng);
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
