package meboo.com.itrouser.presenter.impl;
import meboo.com.itrouser.base.BasePresenterImpl;
import meboo.com.itrouser.presenter.SettingPresenter;
import meboo.com.itrouser.ui.fragment.SettingFragment;
import meboo.com.itrouser.ui.fragment.SettingView;


public class SettingPresenterImpl extends BasePresenterImpl<SettingView> implements SettingPresenter {
    public SettingPresenterImpl(SettingFragment settingFragment) {
        super(settingFragment);
    }
}
