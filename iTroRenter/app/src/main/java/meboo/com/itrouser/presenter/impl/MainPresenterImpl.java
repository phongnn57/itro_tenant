package meboo.com.itrouser.presenter.impl;

import meboo.com.itrouser.base.BasePresenterImpl;
import meboo.com.itrouser.presenter.MainPresenter;
import meboo.com.itrouser.ui.activity.MainView;

/**
 * Created by VietVan on 03/07/2018.
 */

public class MainPresenterImpl extends BasePresenterImpl<MainView> implements MainPresenter {

    public MainPresenterImpl(MainView view) {
        super(view);
    }

}
