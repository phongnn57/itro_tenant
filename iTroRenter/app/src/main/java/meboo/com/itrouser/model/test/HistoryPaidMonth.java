package meboo.com.itrouser.model.test;

import java.util.ArrayList;

public class HistoryPaidMonth {
    String month;
    ArrayList<HistoryPaidDetail> arrayHistoryPaid;

    public HistoryPaidMonth(String month, ArrayList<HistoryPaidDetail> arrayHistoryPaid) {
        this.month = month;
        this.arrayHistoryPaid = arrayHistoryPaid;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public ArrayList<HistoryPaidDetail> getArrayHistoryPaid() {
        return arrayHistoryPaid;
    }

    public void setArrayHistoryPaid(ArrayList<HistoryPaidDetail> arrayHistoryPaid) {
        this.arrayHistoryPaid = arrayHistoryPaid;
    }

}
