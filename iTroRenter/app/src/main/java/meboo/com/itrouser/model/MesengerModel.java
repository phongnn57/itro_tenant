package meboo.com.itrouser.model;

/**
 * Created by VietVan on 23/07/2018.
 */

public class MesengerModel {
    private String location;
    private MesDetailModel last_message;
    private String name;
    private String idAvata;
    private String idFr;

    public String getIdFr() {
        return idFr;
    }

    public void setIdFr(String idFr) {
        this.idFr = idFr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MesengerModel() {
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public MesDetailModel getLastmes() {
        return last_message;
    }

    public void setLastmes(MesDetailModel lastmes) {
        this.last_message = lastmes;
    }

    public String getIdAvata() {
        return idAvata;
    }

    public void setIdAvata(String idAvata) {
        this.idAvata = idAvata;
    }

    public MesDetailModel getLast_message() {
        return last_message;
    }

    public void setLast_message(MesDetailModel last_message) {
        this.last_message = last_message;
    }

    @Override
    public String toString() {
        return "MesengerModel{" +
                "location='" + location + '\'' +
                ", last_message=" + last_message +
                ", name='" + name + '\'' +
                ", idAvata='" + idAvata + '\'' +
                '}';
    }
}
