package meboo.com.itrouser.widget;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by xuanhong on 11/15/17. HappyCoding!
 */

public class MoneyNumberFormattingTextWatcher implements TextWatcher {
    private final int maxLength;
    private EditText editText;
    private String current;

    public MoneyNumberFormattingTextWatcher(EditText editText, int maxLength) {
        this.editText = editText;
        this.maxLength = maxLength;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public synchronized void afterTextChanged(Editable s) {
        if (!s.toString().equals(current)) {
            editText.removeTextChangedListener(this);

            String cleanString = s.toString().replaceAll("[$,.]", "");

            Locale locale = new Locale("vi", "VN");
            NumberFormat numberFormat = NumberFormat.getIntegerInstance(locale);
            long number = 0;
            try {
                number = Long.parseLong(cleanString);
            } catch (NumberFormatException ignored) {
            }
            String formatted = numberFormat.format(number);
            if (formatted.length() <= maxLength) {
                editText.setText(formatted.toCharArray(), 0, formatted.length());
                editText.setSelection(formatted.length());
            }

            editText.addTextChangedListener(this);
        }
    }
}