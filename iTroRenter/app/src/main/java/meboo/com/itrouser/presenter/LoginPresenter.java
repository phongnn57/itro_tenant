package meboo.com.itrouser.presenter;


import meboo.com.itrouser.base.BasePresenter;

public interface LoginPresenter extends BasePresenter {
    void reFreshToken();

    void login();
}
