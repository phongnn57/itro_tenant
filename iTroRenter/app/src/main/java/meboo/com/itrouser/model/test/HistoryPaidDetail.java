package meboo.com.itrouser.model.test;

import java.util.ArrayList;

public class HistoryPaidDetail {
    String id,content, name;
    String  amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HistoryPaidDetail(String id, String amount, String content, String name) {

        this.id = id;
        this.amount = amount;
        this.content = content;
        this.name = name;
    }
}
