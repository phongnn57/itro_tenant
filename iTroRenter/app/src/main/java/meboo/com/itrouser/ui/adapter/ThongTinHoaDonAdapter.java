package meboo.com.itrouser.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import meboo.com.itrouser.R;
import meboo.com.itrouser.model.test.Payment;
import meboo.com.itrouser.ui.activity.ChiTietHoaDonActivity;

public class ThongTinHoaDonAdapter extends RecyclerView.Adapter<ThongTinHoaDonAdapter.ViewHolder> {
    Context context;
    ArrayList<Payment> arrayPayment;

    public ThongTinHoaDonAdapter(Context context, ArrayList<Payment> arrayPayment) {
        this.context = context;
        this.arrayPayment = arrayPayment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_ds_hoa_don_2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvPaid.setVisibility(View.GONE);
        DecimalFormat decimalFormat = new DecimalFormat("###,###");
        holder.tvAmount.setText(decimalFormat.format(arrayPayment.get(position).getAmount()));
        holder.tvId.setText("HD" + arrayPayment.get(position).getId().toString());
        holder.tvRemain.setText(decimalFormat.format(arrayPayment.get(position).getRemain()));
        if (arrayPayment.get(position).getRemain() == 0) {
            holder.tvPaid.setVisibility(View.VISIBLE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChiTietHoaDonActivity.class);
                intent.putExtra("test", 1);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayPayment.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvPaid, tvId, tvRemain, tvAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            tvPaid = itemView.findViewById(R.id.tv_paid);
            tvId = itemView.findViewById(R.id.tv_id);
            tvRemain = itemView.findViewById(R.id.tv_remain);
            tvAmount = itemView.findViewById(R.id.tv_amount);
        }
    }
}
