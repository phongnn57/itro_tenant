package meboo.com.itrouser.presenter.impl;


import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import meboo.com.itrouser.Key;
import meboo.com.itrouser.base.BasePresenterImpl;
import meboo.com.itrouser.model.Account;
import meboo.com.itrouser.model.Data;
import meboo.com.itrouser.network.NetworkModule;
import meboo.com.itrouser.presenter.LoginPresenter;
import meboo.com.itrouser.ui.activity.LoginView;
import meboo.com.itrouser.util.PrefUtil;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginPresenterImpl extends BasePresenterImpl<LoginView> implements LoginPresenter {
    private String TAG = "LoginPresenterImpl";
    private String mUser, mPass, mes;
    private FirebaseAuth mAuth;

    public LoginPresenterImpl(LoginView view) {
        super(view);
        mAuth = FirebaseAuth.getInstance();
    }


    @Override
    public void reFreshToken() {
        String token = "Bearer " + PrefUtil.getDataUser(getView().gContext()).getToken();
        NetworkModule.getService().refreshToken(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().showLogin();
                        Log.i(TAG, "onError: " + e.toString());
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        Log.i(TAG, "onNext: " + jsonObject.toString());
                        if (jsonObject.get("status").toString().equals("1")) {
                            Data data = PrefUtil.getDataUser(getView().gContext());
                            data.setToken(jsonObject.getAsJsonObject("data").getAsJsonPrimitive("token").getAsString());
                            PrefUtil.saveDataUser(data, getView().gContext());
                            getView().login();
                        } else {
                            getView().showLogin();
                        }
                    }
                });
    }


    @Override
    public void login() {
        if (!checkInput()) return;
        getView().addWait();
        String token = FirebaseInstanceId.getInstance().getToken();
        if (token == null || token.isEmpty()) {
            token = "null123456";
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("phone", mUser);
        params.put("password", mPass);
        params.put("device_token_android", token);
        params.put("app_type", "RENTER");
        Log.d(TAG, "login: " + params);
        NetworkModule.getService().login(params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Account>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "onCompleted: ");
            }

            @Override
            public void onError(Throwable e) {
                getView().removeWait();
                getView().dismissErro();
                getView().onFailure("Đăng nhập không thành công!");
                Log.d(TAG, "onError: " + e.toString());

            }

            @Override
            public void onNext(Account account) {
                Log.d(TAG, "onNext: " + account.toString());
                getView().removeWait();
                PrefUtil.saveDataUser(account.getData(), getView().getPrActivity());
                if (account.getCode() == 1) {
                    String token = account.getData().getFirebaseToken();
                    Log.d(TAG, "onNext: token:    " + token);
                    getView().login();
                    mAuth.signInWithCustomToken(token)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "signInWithCustomToken:success");
                                        FirebaseUser user = mAuth.getCurrentUser();


                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "signInWithCustomToken:failure", task.getException());
                                    }
                                }
                            });
                } else getView().onFailure(account.getMessage());
            }
        });
    }

    private boolean checkInput() {
        mUser = getView().getUser();
        mPass = getView().getPassword();
        mes = "Bạn chưa nhập ";
        if (mPass.isEmpty()) {
            mes = mes + "mật khẩu";

            if (mUser.isEmpty()) {
                mes = mes + " và số điện thoại ";
                getView().onFailure(mes);
                getView().requestFocus(Key.USER_NAME);
                return false;
            }
            getView().onFailure(mes);
            getView().requestFocus(Key.PASS_WORD);
            return false;
        }
        if (mUser.isEmpty()) {
            getView().requestFocus(Key.USER_NAME);
            mes = mes + "số điện thoại";
            getView().onFailure(mes);
            return false;
        }

        return true;
    }
}
