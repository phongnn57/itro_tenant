package meboo.com.itrouser.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseFragment;
import meboo.com.itrouser.presenter.SettingPresenter;
import meboo.com.itrouser.presenter.impl.SettingPresenterImpl;
import meboo.com.itrouser.util.Logout;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends BaseFragment<SettingPresenter> implements SettingView {

    Unbinder unbinder;

    public static SettingFragment newInstance() {
        return new SettingFragment();
    }

    @Override
    public SettingPresenter createPresenter() {
        return new SettingPresenterImpl(this);
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_setting;
    }

    @Override
    public void initializeComponents(View view) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
