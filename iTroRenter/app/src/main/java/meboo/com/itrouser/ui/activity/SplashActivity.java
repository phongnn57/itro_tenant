package meboo.com.itrouser.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.widget.Toast;

import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseActivity;
import meboo.com.itrouser.presenter.SplashPresenter;
import meboo.com.itrouser.presenter.impl.SplashPresenterImpl;

public class SplashActivity extends BaseActivity<SplashPresenter> implements SplashView {
    @Override
    public int getContentViewId() {
        return R.layout.activity_splash;
    }

    @Override
    public void initializeComponents() {
        initPermission();
              getPresenter().reFreshToken();

    }

    @Override
    public SplashPresenter createPresenter() {
        return new SplashPresenterImpl(this);
    }

    @Override
    public Context gContext() {
        return this;
    }
    @Override
    public void login() {
        startActivity(MainActivity.getCallingIntent(this));
        this.finish();
    }

    @Override
    public void showLogin() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(LoginActivity.getCallingIntent(SplashActivity.this));
                SplashActivity.this.finish();

            }
        }, 1000);

    }

    public void initPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                //Permisson don't granted
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.CALL_PHONE)) {
                    Toast.makeText(SplashActivity.this, "Permission isn't granted ", Toast.LENGTH_SHORT).show();
                }
                // Permisson don't granted and dont show dialog again.
                else {
                    Toast.makeText(SplashActivity.this, "Permisson don't granted and dont show dialog again ", Toast.LENGTH_SHORT).show();
                }
                //Register permission
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(SplashActivity.this, "Permision Write File is Granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(SplashActivity.this, "Permision Write File is Denied", Toast.LENGTH_SHORT).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
