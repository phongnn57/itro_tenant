package meboo.com.itrouser.presenter.impl;

import meboo.com.itrouser.base.BasePresenterImpl;
import meboo.com.itrouser.presenter.ChiTietHoaDonPresenter;
import meboo.com.itrouser.ui.activity.ChiTietHoaDonView;

public class ChiTietHoaDonPresenterImp extends BasePresenterImpl<ChiTietHoaDonView> implements ChiTietHoaDonPresenter {
    public ChiTietHoaDonPresenterImp(ChiTietHoaDonView view) {
        super(view);
    }
}
