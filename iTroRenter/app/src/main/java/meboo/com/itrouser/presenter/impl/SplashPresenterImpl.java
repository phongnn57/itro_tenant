package meboo.com.itrouser.presenter.impl;

import android.util.Log;

import com.google.gson.JsonObject;

import meboo.com.itrouser.base.BasePresenterImpl;
import meboo.com.itrouser.model.Data;
import meboo.com.itrouser.network.NetworkModule;
import meboo.com.itrouser.presenter.SplashPresenter;
import meboo.com.itrouser.ui.activity.SplashView;
import meboo.com.itrouser.util.PrefUtil;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SplashPresenterImpl extends BasePresenterImpl<SplashView> implements SplashPresenter {
    private static final String TAG = "SplashPresenterImpl";

    public SplashPresenterImpl(SplashView view) {
        super(view);
    }

    @Override
    public void reFreshToken() {
        Log.i(TAG, "reFreshToken: " + String.valueOf(PrefUtil.getDataUser(getView().gContext()) == null));
        if (PrefUtil.getDataUser(getView().gContext()) == null) {
            getView().showLogin();
        } else {
            String token = "Bearer " + PrefUtil.getDataUser(getView().gContext()).getToken();
            NetworkModule.getService().refreshToken(token)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<JsonObject>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            getView().showLogin();
                            Log.i(TAG, "onError: " + e.toString());
                        }

                        @Override
                        public void onNext(JsonObject jsonObject) {
                            Log.i(TAG, "onNext: " + jsonObject.toString());
                            if (jsonObject.get("status").toString().equals("1")) {
                                Data data = PrefUtil.getDataUser(getView().gContext());
                                data.setToken(jsonObject.getAsJsonObject("data").getAsJsonPrimitive("token").getAsString());
                                PrefUtil.saveDataUser(data, getView().gContext());
                                getView().login();
                            } else {
                                getView().showLogin();
                            }
                        }
                    });
        }
    }
}
