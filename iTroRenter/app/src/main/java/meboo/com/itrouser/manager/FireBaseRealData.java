package meboo.com.itrouser.manager;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import meboo.com.itrouser.model.InfoGroup;
import meboo.com.itrouser.model.MesDetailModel;
import meboo.com.itrouser.model.MesengerModel;

/**
 * Created by Blue on 1/12/2018.
 */

public class FireBaseRealData {
    private static final String TAG = "FIREBASEREALCLASS";
    private static FireBaseRealData instance;

    public FireBaseRealData() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRefUser = database.getReference("users");

        DatabaseReference myRefConversations = database.getReference("conversations");
    }

    public static FireBaseRealData getInstance() {
        if (instance == null) {
            instance = new FireBaseRealData();
        }
        return instance;
    }

    public void getRoomAB(final String A, final String B, OnListennerDataCallBack onListennerDataCallBack) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRefUser = database.getReference("users");
        Log.d(TAG, "getRoomAB: " + A + "  " + B);
        myRefUser.child(A).child("conversations").child(B).child("location").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onChildAdded:getPageSize 1 " + dataSnapshot.getKey() + dataSnapshot);

                onListennerDataCallBack.onRoom(dataSnapshot.getValue() == null ? null : dataSnapshot.getValue().toString());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: ");
                onListennerDataCallBack.onRoom(null);
            }
        });

    }

    public void getUsersconversations(String idA, OnListennerDataCallBack onListennerDataCallBack) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRefUser = database.getReference("users");

        myRefUser.child(idA).child("conversations").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "onChildAdded123: " + dataSnapshot.getValue()+"    "+dataSnapshot.getKey());
                MesengerModel mes = dataSnapshot.getValue(MesengerModel.class);
                mes.setIdFr(dataSnapshot.getKey());

//                JsonParser parser = new JsonParser();
//                JsonObject o = parser.parse(dataSnapshot.getValue().toString()).getAsJsonObject();
//                Log.d(TAG, "onChildAdded 0: " + o);
                //Log.d(TAG, "onChildAdded mes: " + mes);
               // onListennerDataCallBack.onRoom(o.get("location").toString().replace("\"", ""));
                onListennerDataCallBack.onLastMes(mes);
                // getLastMes2(o.get("location").toString().replace("\"", ""), onListennerDataCallBack, model);


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });

    }


    //
    public void getDataMes(String room, OnListennerDataCallBack onListennerDataCallBack) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRefConversations = database.getReference("conversations");
        myRefConversations.child(room).child("messages").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "onChildAdded: 2" + dataSnapshot.getValue());
                MesDetailModel mes = dataSnapshot.getValue(MesDetailModel.class);

                // MesDetailModel mes2=new Gson().fromJson(dataSnapshot.getValue().toString(), MesDetailModel.class);
                Log.d(TAG, "onChildAdded: " + mes);
                onListennerDataCallBack.onData(mes);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "onChildChanged: " + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onChildRemoved: " + dataSnapshot.toString());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "onChildMoved: " + dataSnapshot.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: ");
            }
        });

    }
    public void loadListGroups(String id,OnListennerDataCallBack onListennerDataCallBack) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRefUserV2 = database.getReference("userv2");
        myRefUserV2.child(id).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Log.d(TAG, "onChildAdded: " + dataSnapshot.getKey() + "/");

                    String key = dataSnapshot.getKey();
                    String avatar = "", name = "";
                if(dataSnapshot.hasChild("info")){

                    avatar = dataSnapshot.child("info").child("avatar").getValue().toString();
                    name = dataSnapshot.child("info").child("name").getValue().toString();
                }

                String content = "",type="", time = "0", read = "",from="";
                if(dataSnapshot.hasChild("last_message")){

                    content = dataSnapshot.child("last_message").child("content").getValue().toString();
                    type = dataSnapshot.child("last_message").child("type").getValue().toString();
                    time = dataSnapshot.child("last_message").child("time").getValue().toString();
                    read = dataSnapshot.child("last_message").child("read").getValue().toString();
                    from = dataSnapshot.child("last_message").child("name").getValue().toString();
                    Log.d(TAG, "onUpdate: "+type);
                }

                InfoGroup info = new InfoGroup(avatar, name, content, key, Long.parseLong(time), Boolean.parseBoolean(read),type,from);
                onListennerDataCallBack.onLoadList(info);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
               onListennerDataCallBack.onUpdate(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
