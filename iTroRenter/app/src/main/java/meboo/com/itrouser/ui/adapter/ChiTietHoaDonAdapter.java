package meboo.com.itrouser.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import meboo.com.itrouser.R;
import meboo.com.itrouser.model.test.PaymentDetail;

public class ChiTietHoaDonAdapter extends RecyclerView.Adapter<ChiTietHoaDonAdapter.ViewHolder> {
    Context context;
    ArrayList<PaymentDetail> arrayPaymentDetail;

    public ChiTietHoaDonAdapter(Context context, ArrayList<PaymentDetail> arrayPaymentDetail) {
        this.context = context;
        this.arrayPaymentDetail = arrayPaymentDetail;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_chi_tiet_hoa_don,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvContent.setText(arrayPaymentDetail.get(position).getContent());
        holder.tvAmount.setText(arrayPaymentDetail.get(position).getAmount());
        holder.tvCount.setText(arrayPaymentDetail.get(position).getCount());
    }

    @Override
    public int getItemCount() {
        return arrayPaymentDetail.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvContent, tvCount, tvAmount;
        public ViewHolder(View itemView) {
            super(itemView);
            tvContent= itemView.findViewById(R.id.tv_content);
            tvCount= itemView.findViewById(R.id.tv_count);
            tvAmount= itemView.findViewById(R.id.tv_amount);
        }
    }
}
