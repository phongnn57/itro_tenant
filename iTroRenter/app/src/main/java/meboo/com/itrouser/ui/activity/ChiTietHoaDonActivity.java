package meboo.com.itrouser.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseActivity;
import meboo.com.itrouser.model.test.PaymentDetail;
import meboo.com.itrouser.presenter.ChiTietHoaDonPresenter;
import meboo.com.itrouser.presenter.impl.ChiTietHoaDonPresenterImp;
import meboo.com.itrouser.ui.adapter.ChiTietHoaDonAdapter;

public class ChiTietHoaDonActivity extends BaseActivity<ChiTietHoaDonPresenter> implements ChiTietHoaDonView {
    Toolbar myToolbar;
    RecyclerView rvChiTietHoaDon;

    @Override
    public int getContentViewId() {
        return R.layout.activity_chi_tiet_hoa_don;
    }

    @Override
    public void initializeComponents() {
        setupToolbar();
        if (getIntent().getIntExtra("test", 0) == 1) {
            rvChiTietHoaDon = findViewById(R.id.rv_chi_tiet_hoa_don);
            rvChiTietHoaDon.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            ArrayList<PaymentDetail> paymentDetails = new ArrayList<>();
            paymentDetails.add(new PaymentDetail("1", "Tiền nhà tháng 9/2018", "5,000,000"));
            paymentDetails.add(new PaymentDetail("2", "Tiền điện, nước tháng 9/2018", "250,000"));
            ChiTietHoaDonAdapter adapter = new ChiTietHoaDonAdapter(this, paymentDetails);
            rvChiTietHoaDon.setAdapter(adapter);
        }
    }

    private void setupToolbar() {
        myToolbar = findViewById(R.id.myToolbar);

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public ChiTietHoaDonPresenter createPresenter() {
        return new ChiTietHoaDonPresenterImp(this);
    }
}
