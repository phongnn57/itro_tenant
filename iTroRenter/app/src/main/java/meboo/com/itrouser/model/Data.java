package meboo.com.itrouser.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by HP on 8/3/2017.
 */

public class Data {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("stat")
    @Expose
    private List<Object> stat = null;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("firebase_token")
    @Expose
    private String firebaseToken;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Object> getStat() {
        return stat;
    }

    public void setStat(List<Object> stat) {
        this.stat = stat;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }
}
