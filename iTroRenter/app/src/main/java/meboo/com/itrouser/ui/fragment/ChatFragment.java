package meboo.com.itrouser.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseFragmentt;

public class ChatFragment extends BaseFragmentt {


    public static ChatFragment chatFragment;

    public static ChatFragment newInstance() {
        if(chatFragment == null)
            chatFragment = new ChatFragment();

        return chatFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        return view;

    }


}
