package meboo.com.itrouser.manager;


import com.google.firebase.database.DataSnapshot;

import meboo.com.itrouser.model.InfoGroup;
import meboo.com.itrouser.model.MesDetailModel;
import meboo.com.itrouser.model.MesengerModel;

/**
 * Created by Blue on 1/12/2018.
 */

public interface OnListennerDataCallBack {
    void onRoom(String room);
    void onData(MesDetailModel data);
    void onLastMes(MesengerModel model);
    void onLoadList(InfoGroup group);
    void onUpdate(DataSnapshot dataSnapshot);
}
