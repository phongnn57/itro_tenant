package meboo.com.itrouser;

import java.util.Locale;


public class GlobalConst {


    public static final Locale LOCALE_VN = new Locale("vi", "VN");
    public static final String[] SEX = {"NỮ", "NAM"};
    public static final String[] TYPEHOST = {"Nhà tầng", "Chung cư mini","Dãy trọ","Nhà cấp 4"};
    public static final String MYID = "456";
    public static final String FRID = "123";
    public static final int REQUEST_CODE = 14;
    public static final String[] LTYPE={"Giờ tự do","Không chung chủ","Vệ sinh khép kín","Cho nấu ăn", "Bếp nấu ăn","Nước máy",
    "Có gác lửng","Chỗ để xe","Chỗ phơi đồ","Ban công","Giường","Tủ quần áo","Khóa cổng","Internet","Truyền hình cap","Điều hòa","Nóng lạnh",
    "Tivi","Tủ lạnh","Máy giặt","Quạt","Điện nước","Camera chống trộm","Hợp đồng"};
    public static final String THIEU="Vui lòng nhập ";
    public static final String ACTION_FLOOR = "actionfloor";
    public static final String KEY_NUMBER_KQ = "numberkq";
    public static final String KEY_IMAGE_KQ = "imagekq";
    public static final String ACTION_IMAGE = "actitonimage";
    public static final String ACTION_ROOMTYPE = "roomtypeaction";


    public static final String ACTION_CLOSE = "closeact";
}
