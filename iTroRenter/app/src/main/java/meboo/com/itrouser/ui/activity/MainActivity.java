package meboo.com.itrouser.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import com.roughike.bottombar.BottomBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseActivity;
import meboo.com.itrouser.presenter.MainPresenter;
import meboo.com.itrouser.presenter.impl.MainPresenterImpl;
import meboo.com.itrouser.ui.adapter.MainPagerAdapter;

public class MainActivity extends BaseActivity<MainPresenter> implements MainView, View.OnClickListener {

    @BindView(R.id.content)
    ViewPager mContent;
    @BindView(R.id.bottom_bar)
    BottomBar bottomBar;
    private MainPagerAdapter mainPagerAdapter;

    @Override
    public void onClick(View v) {

    }
    public static Intent getCallingIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenterImpl(this);
    }

    @Override
    public void HomeClick() {
        mContent.setCurrentItem(MainPagerAdapter.HOME_INDEX);
    }


    @Override
    public void ChatClick() {
        mContent.setCurrentItem(MainPagerAdapter.CHAT_INDEX);
    }


    @Override
    public void SettingClick() {
        mContent.setCurrentItem(MainPagerAdapter.SETTING_INDEX);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    public void initializeComponents() {
        ButterKnife.bind(this);
        int page=getIntent().getIntExtra("page",0);
        Log.d("initializeComponents", "initializeComponents: "+page);
        mContent.setCurrentItem(page);
        setupBottomBar();
        setupViewPager();
    }

    private void setupViewPager() {

        mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mContent.setAdapter(mainPagerAdapter);
        mContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottomBar.selectTabAtPosition(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mContent.setOffscreenPageLimit(3);
    }

    private void setupBottomBar() {
        bottomBar.setOnTabSelectListener(tabId -> {
            switch (tabId) {
                case R.id.navigation_home:
                    HomeClick();
                    break;
                case R.id.navigation_contact:
                    ChatClick();
                    break;
                case R.id.navigation_setting:
                    SettingClick();
                    break;
                default:
                    break;
            }
        }, false);
    }

    @Override
    public void onBackPressed() {
    }
}
