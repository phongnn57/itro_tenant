package meboo.com.itrouser.ui.fragment;

import meboo.com.itrouser.base.BaseView;
import meboo.com.itrouser.presenter.SettingPresenter;


public interface SettingView extends BaseView<SettingPresenter> {
}
