package meboo.com.itrouser.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import meboo.com.itrouser.R;
import meboo.com.itrouser.model.test.Payment;
import meboo.com.itrouser.model.test.PaymentMonth;

public class DanhSachHoaDonAdapter extends RecyclerView.Adapter<DanhSachHoaDonAdapter.ViewHolder>{
    Context context;
    ArrayList<PaymentMonth> arrayPaymentMonth;

    public DanhSachHoaDonAdapter(Context context, ArrayList<PaymentMonth> arrayPaymentMonth) {
        this.context = context;
        this.arrayPaymentMonth = arrayPaymentMonth;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_ds_hoa_don,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvMonth.setText(arrayPaymentMonth.get(position).getTime());
        ThongTinHoaDonAdapter adapter = new ThongTinHoaDonAdapter(context, (ArrayList<Payment>) arrayPaymentMonth.get(position).getHistories());
        holder.rvChiTietHoaDon.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return arrayPaymentMonth.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvMonth;
        RecyclerView rvChiTietHoaDon;
        public ViewHolder(View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            rvChiTietHoaDon = itemView.findViewById(R.id.rv_chi_tiet_hoa_don);
            rvChiTietHoaDon.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        }
    }
}
