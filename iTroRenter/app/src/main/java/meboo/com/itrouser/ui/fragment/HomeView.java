package meboo.com.itrouser.ui.fragment;

import android.content.Context;

import meboo.com.itrouser.base.BaseView;
import meboo.com.itrouser.presenter.HomePresenter;

/**
 * Created by VietVan on 03/07/2018.
 */

public interface HomeView extends BaseView<HomePresenter> {

    Context gContext();
}
