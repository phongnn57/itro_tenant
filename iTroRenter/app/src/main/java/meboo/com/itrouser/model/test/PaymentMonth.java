package meboo.com.itrouser.model.test;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentMonth {
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("histories")
    @Expose
    private List<Payment> histories = null;

    public PaymentMonth(String time, List<Payment> histories) {
        this.time = time;
        this.histories = histories;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<Payment> getHistories() {
        return histories;
    }

    public void setHistories(List<Payment> histories) {
        this.histories = histories;
    }
}
