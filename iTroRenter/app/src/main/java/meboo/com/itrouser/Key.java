package meboo.com.itrouser;

/**
 * Created by Blue on 1/10/2018.
 */

public interface Key {
    static final int PRO = 60;
    static final int DIS = 61;
    static final int WAR = 62;
    String USER_NAME = "username";
    String FILTER = "filter";
    String PASS_WORD = "password";
    String ARRIMG = "arrimg";
    String ROOM_ID = "roomId";
    String ROOMNAME = "roomname";
    String POSITION = "position";
    String CONTRACT_ID = "contractId";
    int TAB_LOGIN = 1;
    int TAB_REGISTER = 2;
    int IMG_TONGQUAN = 3;
    int IMG_CONGRAVAO = 4;
    int IMG_HANHLANG = 5;
    int IMG_CHODEXE = 6;
    int IMG_CUAPHONG = 7;
    int IMG_TRONGPHONG1 = 8;
    int IMG_TRONGPHONG2 = 9;
    int IMG_TRONGPHONG3 = 10;
    int IMG_GACXEP = 11;
    int IMG_BEP = 12;
    int IMG_NHAVESINH = 13;
    public static final String APP_PREFERENCE = "itrohostandroid";
    public static final String DATA_USER = "datauser";
    public static final String IDB = "idb";
    public static final String IDGROUP = "idgroup";
    public static final String FILTER_USER = "filteruser";
    public static final String FILTER_COSO = "filtercoso";
    public static final String MATKHAU = "password";
    public static final String USERNAME = "username";
    public static final String LANGUAGE = "language";
    public static final String HOSTEL_ID = "hostelId";
    public static final String HOSTEL_NAME = "hostelName";
    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_STATUS = 1;
    public static final String RESULT = "result";
    public static final String USER_DETAIL = "userDetail";
    public static final String STATUS = "status";
    int collect = 4;
    int pay = 5;
    int other = 6;
    int diennuoc = 7;
    String NOTIF = "notif";
    String LOCATION = "locationnow";
    String PROID = "proid";
    String DISID = "disid";


}
