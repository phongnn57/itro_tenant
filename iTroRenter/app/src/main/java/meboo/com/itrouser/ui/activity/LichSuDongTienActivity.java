package meboo.com.itrouser.ui.activity;

import android.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseActivity;
import meboo.com.itrouser.model.test.HistoryPaidDetail;
import meboo.com.itrouser.model.test.HistoryPaidMonth;
import meboo.com.itrouser.presenter.LichSuDongTienPresenter;
import meboo.com.itrouser.presenter.impl.LichSuDongTienPresenterImp;
import meboo.com.itrouser.ui.adapter.LichSuDongTienAdapter;
import meboo.com.itrouser.util.Toolbox;

public class LichSuDongTienActivity extends BaseActivity<LichSuDongTienPresenter> implements LichSuDongTienView, View.OnClickListener {
    ImageView imvFilter;
    RecyclerView rvLichSuDongTien;
    Toolbar myToolbar;
    String startDate, endDate;
    LichSuDongTienAdapter adapter;
    ArrayList<HistoryPaidMonth> arrayLichSuDongTien = new ArrayList<>();

    @Override
    public int getContentViewId() {
        return R.layout.activity_lich_su_dong_tien;
    }

    @Override
    public void initializeComponents() {
        init();
        setFakeData();

        adapter = new LichSuDongTienAdapter(this, arrayLichSuDongTien);
        rvLichSuDongTien.setAdapter(adapter);
    }

    private void setFakeData() {
        ArrayList<HistoryPaidDetail> array = new ArrayList<>();
        array.add(new HistoryPaidDetail("HD10101010", "1,000,000", "Đóng tiền nhà tháng 8", "Trần Anh Minh"));
        array.add(new HistoryPaidDetail("HD10101020", "200,000", "Đóng tiền nước tháng 8", "Trần Anh Minh"));
        ArrayList<HistoryPaidDetail> array2 = new ArrayList<>();
        array2.add(new HistoryPaidDetail("HD20201210", "2,000,000", "Đóng tiền nhà tháng 7", "Trần Anh Minh"));
        arrayLichSuDongTien.add(new HistoryPaidMonth("8/2018", array));
        arrayLichSuDongTien.add(new HistoryPaidMonth("7/2018", array2));
    }

    private void init() {
        imvFilter = findViewById(R.id.imv_filter);
        rvLichSuDongTien = findViewById(R.id.rv_lich_su_dong_tien);
        myToolbar = findViewById(R.id.myToolbar);

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imvFilter.setOnClickListener(this);
        rvLichSuDongTien.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }


    @Override
    public LichSuDongTienPresenter createPresenter() {
        return new LichSuDongTienPresenterImp(this);
    }

    @Override
    public void onClick(View v) {
        showDialog();
    }

    private void showDialog() {
        Calendar now = Calendar.getInstance();
        int month = now.get(Calendar.MONTH) + 1;
        int year = now.get(Calendar.YEAR);
        int startMonth, startYear;
        if (month - 3 > 0) {
            startMonth = month - 3;
            startYear = year;

        } else {
            startMonth = 12 + (month - 3);
            startYear = year - 1;
        }

        startDate = startMonth + "/" + startYear;
        endDate = month + "/" + year;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_filter_history_paid_money, null);
        builder.setView(view);
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();

        TextView txtDateFrom = view.findViewById(R.id.txt_date_from);
        TextView txtDateTo = view.findViewById(R.id.txt_date_to);
        ImageView btnDatePickerFrom = view.findViewById(R.id.btn_date_picker_from);
        ImageView btnDatePickerTo = view.findViewById(R.id.btn_date_picker_to);


        txtDateFrom.setText(startDate);
        txtDateTo.setText(endDate);
        btnDatePickerFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toolbox.showDatePickerMonthYearDialog(LichSuDongTienActivity.this, null, null, new Toolbox.OnDateSetListener() {
                    @Override
                    public void onDateSet(int year, int month, int dayOfMonth, Calendar calendar) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
                        startDate = dateFormat.format(calendar.getTime());
                        txtDateFrom.setText(startDate);
                    }
                });
            }
        });

        btnDatePickerTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toolbox.showDatePickerMonthYearDialog(LichSuDongTienActivity.this, null, null, new Toolbox.OnDateSetListener() {
                    @Override
                    public void onDateSet(int year, int month, int dayOfMonth, Calendar calendar) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
                        endDate = dateFormat.format(calendar.getTime());
                        txtDateTo.setText(endDate);
                    }
                });
            }
        });

        view.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
