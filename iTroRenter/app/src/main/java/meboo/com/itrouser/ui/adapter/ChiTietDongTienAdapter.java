package meboo.com.itrouser.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import meboo.com.itrouser.R;
import meboo.com.itrouser.model.test.HistoryPaidDetail;

public class ChiTietDongTienAdapter extends RecyclerView.Adapter<ChiTietDongTienAdapter.ViewHolder>{
    Context context;
    ArrayList<HistoryPaidDetail> arrayHistoryPaidDetail;

    public ChiTietDongTienAdapter(Context context, ArrayList<HistoryPaidDetail> arrayHistoryPaidDetail) {
        this.context = context;
        this.arrayHistoryPaidDetail = arrayHistoryPaidDetail;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_lich_su_dong_tien_2,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvAmount.setText(arrayHistoryPaidDetail.get(position).getAmount());
        holder.tvId.setText(arrayHistoryPaidDetail.get(position).getId().toString());
        holder.tvContent.setText("Nội dung: " + arrayHistoryPaidDetail.get(position).getContent());
        holder.tvName.setText("Người gửi: "+arrayHistoryPaidDetail.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return arrayHistoryPaidDetail.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvContent, tvId, tvName, tvAmount;
        public ViewHolder(View itemView) {
            super(itemView);
            tvContent = itemView.findViewById(R.id.tv_content);
            tvId = itemView.findViewById(R.id.tv_id);
            tvName = itemView.findViewById(R.id.tv_name);
            tvAmount = itemView.findViewById(R.id.tv_amount);
        }
    }
}
