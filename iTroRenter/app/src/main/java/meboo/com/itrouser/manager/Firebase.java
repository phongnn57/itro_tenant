package meboo.com.itrouser.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import meboo.com.itrouser.model.Group;
import meboo.com.itrouser.model.InfoGroup;
import meboo.com.itrouser.model.MesDetailModel;
import meboo.com.itrouser.model.MesengerModel;
import meboo.com.itrouser.util.PrefUtil;
import meboo.com.itrouser.util.Util;

/**
 * Created by Blue on 8/17/2017.
 */

public class Firebase {
    private static final String TAG = "FIREBASECLASS";
    private static Firebase instance;
    DatabaseReference myRefUser;
    DatabaseReference myRefConversations;
    DatabaseReference myRefRoot;
    DatabaseReference myRefUserv2;
    FirebaseDatabase database;

    public Firebase() {
        database = FirebaseDatabase.getInstance();
        myRefUser = database.getReference("users");
        myRefConversations = database.getReference("conversations");
        myRefUserv2 = database.getReference("userv2");
        myRefRoot = database.getReference();
    }

    public static Firebase getInstance() {
        if (instance == null) {
            instance = new Firebase();
        }
        return instance;
    }

    public void postStt(MesDetailModel post, String A, String B, OnListenerSuccessSendMessenger onListenerSuccessSenMessenger) {
        Log.d(TAG, "postStt: ");
        final String[] key = new String[1];
        FireBaseRealData.getInstance().getRoomAB(A, B, new OnListennerDataCallBack() {
            @Override
            public void onRoom(String room) {
                if (room == null || room.isEmpty()) {
                    key[0] = myRefConversations.child("posts").push().getKey();
                    onListenerSuccessSenMessenger.onRoomNull(key[0]);
                } else key[0] = room;

                postSttStep1(post, A, B, key[0], onListenerSuccessSenMessenger);
            }

            @Override
            public void onData(MesDetailModel data) {

            }

            @Override
            public void onLastMes(MesengerModel model) {

            }

            @Override
            public void onLoadList(InfoGroup group) {

            }

            @Override
            public void onUpdate(DataSnapshot dataSnapshot) {

            }
        });


        Log.d(TAG, "postStt: " + key[0]);


    }

    private void postSttStep1(MesDetailModel post, String A, String B, String key, OnListenerSuccessSendMessenger onListenerSuccessSenMessenger) {
        myRefConversations.child(key).child("messages").push().setValue(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {


                postSttStep2(key, B, A, new OnListenerSuccessSendMessenger() {
                    @Override
                    public void onSucccess(Void v) {
                        onListenerSuccessSenMessenger.onSucccess(aVoid);
                    }

                    @Override
                    public void onFail(String v) {
                        onListenerSuccessSenMessenger.onFail(v);
                    }

                    @Override
                    public void onRoomNull(String room) {

                    }
                });

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure: ");
                onListenerSuccessSenMessenger.onFail(e.getMessage());
            }
        });
        myRefUser.child(A).child("conversations").child(B).child("last_message").setValue(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
        myRefUser.child(B).child("conversations").child(A).child("last_message").setValue(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }

    private void postSttStep2(String key, String A, String B, OnListenerSuccessSendMessenger onListenerSuccessSenMessenger) {
        Log.d(TAG, "postStt: ");


        myRefUser.child(A).child("conversations").child(B).child("location").setValue(key).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                postSttStep3(key, A, B, new OnListenerSuccessSendMessenger() {
                    @Override
                    public void onSucccess(Void v) {
                        onListenerSuccessSenMessenger.onSucccess(aVoid);
                    }

                    @Override
                    public void onFail(String v) {
                        onListenerSuccessSenMessenger.onFail(v);
                    }

                    @Override
                    public void onRoomNull(String room) {

                    }
                });


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure: ");
                onListenerSuccessSenMessenger.onFail(e.getMessage());
            }
        });


    }

    private void postSttStep3(String key, String A, String B, OnListenerSuccessSendMessenger onListenerSuccessSenMessenger) {
        Log.d(TAG, "postStt: ");


        myRefUser.child(B).child("conversations").child(A).child("location").setValue(key).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                onListenerSuccessSenMessenger.onSucccess(aVoid);


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "onFailure: ");
                onListenerSuccessSenMessenger.onFail(e.getMessage());
            }
        });


    }

    public Bitmap getBitmapFromURI(String uri) {
        File sd = Environment.getExternalStorageDirectory();
        File image = new File(uri);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
        if (bitmap == null) {
            Log.d("!2312312312null", "getBitmapFromURI: ");
        }
        return bitmap;
    }

    public void uploadImg(String uri, OnListenerSuccessUploadImage onListenerSuccessUploadImg) {


        if (uri == null || uri.isEmpty()) {
            onListenerSuccessUploadImg.onSucccess("");
            return;
        }
        StorageReference storageRef;
        storageRef = FirebaseStorage.getInstance("gs://itro-b9c1a.appspot.com").getReference();
        String fileName = Util.creatFileName("jpg");
        StorageReference mountainsRef = storageRef.child("img" + fileName + ".png");//img name with time now

        Uri file = Uri.fromFile(new File(uri));

        UploadTask uploadTask = mountainsRef.putFile(file);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                onListenerSuccessUploadImg.onFail(exception.getMessage());
                Log.d(TAG, "onFailure: ");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Log.d("444444444", "onSuccess: " + taskSnapshot);
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                //   Log.d(TAG, "onSuccess: " + downloadUrl);
                Log.d("4444444444444444", "onSuccess: " + downloadUrl);
                onListenerSuccessUploadImg.onSucccess(downloadUrl.toString());
            }
        });
    }

}
