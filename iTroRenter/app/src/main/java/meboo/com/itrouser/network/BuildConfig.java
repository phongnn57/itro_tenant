package meboo.com.itrouser.network;

/**
 * Created by HP on 7/24/2017.
 */

public final class BuildConfig {

    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.0";
    public static final String BASEURL = "https://itro.vn";

//    public static final String BASEURL = "https://nhatro.fitme.vn";

    public static final int CACHETIME = 432000;
    public static final int LIMIT = 100;
}
