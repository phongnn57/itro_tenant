package meboo.com.itrouser.ui.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import meboo.com.itrouser.Key;
import meboo.com.itrouser.R;
import meboo.com.itrouser.base.BaseActivity;
import meboo.com.itrouser.presenter.LoginPresenter;
import meboo.com.itrouser.presenter.impl.LoginPresenterImpl;
import meboo.com.itrouser.util.PrefUtil;
import meboo.com.itrouser.util.Toolbox;
import meboo.com.itrouser.util.Util;

public class LoginActivity extends BaseActivity<LoginPresenter>
        implements LoginView, View.OnClickListener {

    @BindView(R.id.edt_user)
    EditText edtUser;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.btn_login)
    TextView btnLogin;
    @BindView(R.id.txt_sign_in)
    TextView btnRegister;
    @BindView(R.id.ll_parent)
    RelativeLayout llParent;
    @BindView(R.id.txt_forget_password)
    TextView tvForgetPass;

    public static Intent getCallingIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_login;
    }

    @Override
    public void initializeComponents() {
        ButterKnife.bind(this);
        edtPassword.setText(PrefUtil.getString(this, Key.PASS_WORD, ""));
        edtUser.setText(PrefUtil.getString(this, Key.USER_NAME, ""));
        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        llParent.setOnClickListener(this);
        tvForgetPass.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        edtPassword.setText(PrefUtil.getString(this, Key.PASS_WORD, ""));
        edtUser.setText(PrefUtil.getString(this, Key.USER_NAME, ""));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                Toolbox.hideKeyboard(LoginActivity.this);
                getPresenter().login();
                PrefUtil.saveString(this, Key.USER_NAME, edtUser.getText().toString());
                PrefUtil.saveString(this, Key.PASS_WORD, edtPassword.getText().toString());
                break;
            case R.id.txt_sign_in:

                break;
            case R.id.ll_parent:
                Toolbox.hideKeyboard(LoginActivity.this);
                break;
            case R.id.txt_forget_password:
                Util.showMessenger("Chức năng đang phát triển", LoginActivity.this);
                break;
        }
    }

    @Override
    public void addWait() {
        Util.getIns().showLoadding(this);
    }

    @Override
    public void removeWait() {
        Util.getIns().hideLoadding();
    }

    @Override
    public void selectTab(int i) {

    }

    @Override
    public Context gContext() {
        return this;
    }

    @Override
    public void onFailure(String mes) {
        Util.showMessenger(mes, this);
    }

    @Override
    public void onSuccess(String mes) {
        login();
    }

    @Override
    public void dismissErro() {

    }

    @Override
    public String getUser() {
        return edtUser.getText().toString();
    }

    @Override
    public void requestFocus(String key) {
        if (key == Key.USER_NAME) {
            edtUser.requestFocus();
            return;

        }
        if (key == Key.PASS_WORD) {
            edtPassword.requestFocus();
            return;
        }
    }

    @Override
    public String getPassword() {
        return edtPassword.getText().toString();
    }

    @Override
    public void login() {
        startActivity(MainActivity.getCallingIntent(this));
        this.finish();
    }

    @Override
    public void showLogin() {

    }

    @Override
    public Activity getPrActivity() {
        return this;
    }

    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenterImpl(this);
    }
}
