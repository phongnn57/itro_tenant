package meboo.com.itrouser.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import meboo.com.itrouser.R;
import meboo.com.itrouser.model.test.HistoryPaidMonth;

public class LichSuDongTienAdapter extends RecyclerView.Adapter<LichSuDongTienAdapter.ViewHolder> {
    Context context;
    ArrayList<HistoryPaidMonth> arrayHistoryPaidMonth;

    public LichSuDongTienAdapter(Context context, ArrayList<HistoryPaidMonth> arrayHistoryPaidMonth) {
        this.context = context;
        this.arrayHistoryPaidMonth = arrayHistoryPaidMonth;
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_lich_su_dong_tien, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvMonth.setText(arrayHistoryPaidMonth.get(position).getMonth());
        ChiTietDongTienAdapter adapter = new ChiTietDongTienAdapter(context, arrayHistoryPaidMonth.get(position).getArrayHistoryPaid());
        holder.rvChiTietDongTien.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return arrayHistoryPaidMonth.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvMonth;
        RecyclerView rvChiTietDongTien;

        public ViewHolder(View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            rvChiTietDongTien = itemView.findViewById(R.id.rv_chi_tiet_dong_tien);
            rvChiTietDongTien.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        }
    }
}
