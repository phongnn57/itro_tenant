package meboo.com.itrouser.model.test;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment {
    public Payment(Integer id, Integer amount, Integer remain) {
        this.id = id;
        this.amount = amount;
        this.remain = remain;
    }

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hostel_id")
    @Expose
    private Integer hostelId;
    @SerializedName("room_id")
    @Expose
    private Integer roomId;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("pay")
    @Expose
    private Integer pay;
    @SerializedName("remain")
    @Expose
    private Integer remain;
    @SerializedName("date_action")
    @Expose
    private String dateAction;
    @SerializedName("user_id")
    @Expose
    private Object userId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("contract_id")
    @Expose
    private Integer contractId;
    @SerializedName("date_display")
    @Expose
    private Object dateDisplay;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("start_date")
    @Expose
    private Object startDate;
    @SerializedName("end_date")
    @Expose
    private Object endDate;
    @SerializedName("contract_status")
    @Expose
    private Integer contractStatus;
    @SerializedName("money_info_name")
    @Expose
    private Object moneyInfoName;
    @SerializedName("hostel_name")
    @Expose
    private String hostelName;
    @SerializedName("room_name")
    @Expose
    private String roomName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHostelId() {
        return hostelId;
    }

    public void setHostelId(Integer hostelId) {
        this.hostelId = hostelId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPay() {
        return pay;
    }

    public void setPay(Integer pay) {
        this.pay = pay;
    }

    public Integer getRemain() {
        return remain;
    }

    public void setRemain(Integer remain) {
        this.remain = remain;
    }

    public String getDateAction() {
        return dateAction;
    }

    public void setDateAction(String dateAction) {
        this.dateAction = dateAction;
    }

    public Object getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Object getDateDisplay() {
        return dateDisplay;
    }

    public void setDateDisplay(Object dateDisplay) {
        this.dateDisplay = dateDisplay;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Object getStartDate() {
        return startDate;
    }

    public void setStartDate(Object startDate) {
        this.startDate = startDate;
    }

    public Object getEndDate() {
        return endDate;
    }

    public void setEndDate(Object endDate) {
        this.endDate = endDate;
    }

    public Integer getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(Integer contractStatus) {
        this.contractStatus = contractStatus;
    }

    public Object getMoneyInfoName() {
        return moneyInfoName;
    }

    public void setMoneyInfoName(Object moneyInfoName) {
        this.moneyInfoName = moneyInfoName;
    }

    public String getHostelName() {
        return hostelName;
    }

    public void setHostelName(String hostelName) {
        this.hostelName = hostelName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
