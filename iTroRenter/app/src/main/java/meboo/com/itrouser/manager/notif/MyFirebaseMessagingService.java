package meboo.com.itrouser.manager.notif;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import meboo.com.itrouser.ApplicationContextSingleton;
import meboo.com.itrouser.Key;
import meboo.com.itrouser.R;
import meboo.com.itrouser.ui.activity.LoginActivity;

/**
 * Created by Blue on 1/24/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final int NOTIFICATION_ID = 100;
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Intent inten = new Intent(ApplicationContextSingleton.getInstance().getApplicationContext(), LoginActivity.class);
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, inten, 0);
        Notification notification = new Notification.Builder(this)
                .setAutoCancel(false)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(remoteMessage.getNotification().getTitle()==null?"":remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody()==null?"":remoteMessage.getNotification().getBody())
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pi)
                .setDefaults(Notification.DEFAULT_ALL)
                .build();
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(100, notification);
        Intent intent = new Intent();
        intent.setAction(Key.NOTIF);
        ApplicationContextSingleton.getInstance().getApplicationContext().sendBroadcast(intent);
    }
}
