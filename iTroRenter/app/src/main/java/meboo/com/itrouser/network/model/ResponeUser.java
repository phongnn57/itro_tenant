package meboo.com.itrouser.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import meboo.com.itrouser.model.User;

/**
 * Created by VietVan on 23/07/2018.
 */

public class ResponeUser {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private User user;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ResponeUser{" +
                "status=" + status +
                ", user=" + user +
                '}';
    }
}
