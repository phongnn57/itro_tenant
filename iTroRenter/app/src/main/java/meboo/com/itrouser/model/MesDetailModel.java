package meboo.com.itrouser.model;

import java.util.ArrayList;

/**
 * Created by VietVan on 23/07/2018.
 */

public class MesDetailModel {
    public String name, image;
    private boolean isRead;
    private String fromID;
    private String toID;
    private Long timestamp;
    private String content;
    private String type;
    private ArrayList<String> attachments;

    public MesDetailModel() {
    }

    public MesDetailModel(String fromID, String toID, Long timestamp, String content, String type1, String name, String image) {

        this.fromID = fromID;
        this.toID = toID;
        this.timestamp = timestamp;
        this.content = content;
        this.type = type1;
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "MesDetailModel{" +
                "isRead=" + isRead +
                ", fromID='" + fromID + '\'' +
                ", toID='" + toID + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", content='" + content + '\'' +
                ", type='" + type + '\'' +
                ", attachments=" + attachments +
                '}';
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getFromID() {
        return fromID;
    }

    public void setFromID(String fromID) {
        this.fromID = fromID;
    }

    public String getToID() {
        return toID;
    }

    public void setToID(String toID) {
        this.toID = toID;
    }


    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
