package meboo.com.itrouser.presenter.impl;

import meboo.com.itrouser.base.BasePresenterImpl;
import meboo.com.itrouser.presenter.HomePresenter;
import meboo.com.itrouser.ui.fragment.HomeView;

/**
 * Created by VietVan on 03/07/2018.
 */

public class HomePresenterImpl extends BasePresenterImpl<HomeView> implements HomePresenter {
    private final String TAG = "HomeFrag";

    public HomePresenterImpl(HomeView view) {
        super(view);
    }


}
