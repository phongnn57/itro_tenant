package meboo.com.itrouser.manager;

/**
 * Created by Blue on 1/12/2018.
 */

public interface OnListenerSuccessSendMessenger {
    void onSucccess(Void v);
    void onFail(String v);
    void onRoomNull(String room);
}
