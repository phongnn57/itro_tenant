package meboo.com.itrouser.util;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;

/**
 * Created by Blue on 8/16/2017.
 */

public class EdittextUtil extends AppCompatEditText {
    private static final String TAG="KEYTEXT";
    public EdittextUtil(Context context) {
        super(context);
    }

    public EdittextUtil(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EdittextUtil(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.i(TAG, "onKeyDown: ");
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.i(TAG, "onKeyUp: ");
        String text = this.getText().toString();
        text = text.replace(".", "");
        text = text.replace("V", "");
        text = text.replace("Đ", "");
        text = text.replace("N", "");
        text = text.replace(" ", "");
        if (text.isEmpty()||text.length()==0){
            this.setText("");
            return super.onKeyUp(keyCode, event);
        }
        this.setText(Formatter.costToVnd(text));
        this.setSelection(this.length()-4);
        return super.onKeyUp(keyCode, event);
    }


}
